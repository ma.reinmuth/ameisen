vector stuff


Scalar product for angle (in float):

$$ 
\text{cos }\varphi = \frac{\vec{u}\circ\vec{v}}{\left|\vec{u}\right|\cdot\left|\vec{v}\right|} \qquad \rightarrow \qquad \varphi = \text{cos }^{-1}\left(\frac{\vec{u}\circ\vec{v}}{\left|\vec{u}\right|\cdot\left|\vec{v}\right|}\right) \ 
$$

u = default vector: \begin{pmatrix} 0 \newline  -1 \end{pmatrix}
v = vector from origin ant to influencing ant: \begin{pmatrix} x \newline  y \end{pmatrix}

**Vector:**

Cross product for distance vector:

$$
\vec{d\_{A0 - A1}} = \vec{A1} \times \vec{A0} =
\begin{pmatrix}
row\_{A0} \newline  col\_{A1}
\end{pmatrix}
\times
\begin{pmatrix}
row\_{A0} \newline  col\_{A1}
\end{pmatrix}
$$

Normalization, reduce the distance vectors length to 1, then we can easily deduce the direction:

1) Length:
$$
|\vec{d\_{A0 - A1}}| = \sqrt{(row\_{A0-A1})² + (col\_{A0-A1})²}
$$
2) Reduce length to 1:
$$
\begin{vmatrix} \frac{1}{|\vec{d\_{A0 - A1}}|} \cdot \vec{d\_{A0 - A1}} \end{vmatrix}
$$