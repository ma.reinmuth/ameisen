---
title: Download
---

# Live Version:

Download our .jar file and test the simulation yourself.

Get the.jar [__**here**__](https://gitlab.com/ma.reinmuth/ameisen/raw/master/export/ABM_ants.jar)

To start it open a terminal or cmd and type in:
```sh
$ java -jar ABM_ants.jar
```