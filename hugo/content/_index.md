---
title: Intro
---
[<img src="http://pngimg.com/uploads/ant/ant_PNG19367.png" style="max-width:15%;min-width:40px;float:right;" alt="Gitlab repo" />](https://gitlab.com/ma.reinmuth/ameisen)

# Ants simulation project

This is the documentation to simulate the search for food of ants using an agent-based modelling approach. The project is part of the geogrpahy class _Introduction to Computer Science for geographers_ in the summer term 2018.

```
Term: Summer 2018

Teacher: Dr. Westerholt, René

Department of Geography

University of Heidelberg
```


See the project on [Gitlab](https://gitlab.com/ma.reinmuth/ameisen)
