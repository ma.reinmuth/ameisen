---
title: functions
---

# Workflow

The general workflow of our model is described in the main class.

The class [movement](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Movement.java) serves as a kind of helper class. It provides all functions to calculate and execute the movement of ants.


The overall workflow of our model:
<img src="https://gitlab.com/ma.reinmuth/ameisen/raw/master/hugo/content/img/flow.png" width="75%" height="75%">


## [calcWeightAnt](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Movement.java#L12)

Depending on the distance form one ant to another and the range of attraction, the weight of influence by that ant is measured.

## [calcWeightFood](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Movement.java#L18)

Depending on the distance to the food and the size of the territory as range, the weight of influence by food is measured.

## [directionAngle](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Movement.java#L23)

Implementation of the formular described in [Concepts](https://ma.reinmuth.gitlab.io/ameisen/concepts/)

Based on the mean angle of other ants in range and the sum of their weight normalized by the weight and angle of the food the movement direction is calculated.

## [calcAntsMove](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Movement.java#L115)

This function executes the calculation of the movement for the next timestep for every ant. 
First a condition tests for one ant to every other if there is one or more in range of attraction. Then the angles, distances and weights/influences are calculated. Finally all this values get fed into the directionAngle function. The result gets translated to X and Y coordinates and stored as next location attribute for each ant.

## [doMove](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Movement.java#L214)

This function is executed after all new locations for the next timestep are calculated. 
The main purpose is to prohibit movement conflicts when two ants want to approach the same cell. Also the cell attribute occupation gets updated in order to provide up to date information when the territory gets printed.
If an ant wants to move to a cell which is already occupied, the ant waits in the current location. After the movement each ants age is updated by the current timestep value. The last task in this function is to check if any of the ants made it to the food. If this is the case this ant gets removed from the simulation and put into a list of survived ants.


