---
title: Classes
---
# Description of classes

## [Territory class](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Territory.java)
In order to create the space where the agent-based model of ants on search of food is running, a territory was established. By definition, the territory includes 50 * 50 cells (which are then defined in the [Cell class](../blob/master/src/datamodel/Cell.java)).
The constructor `public class Territory` initiates several variables for further processing. Every initiated variable is implemented as getter and setter method, so no control is granted to the public and only one entry and one exit point is available to modify the values of their respective variables.  
At first, the `integer` variable `private int size` is defined, which configures the size of the area. The `private int [] foodLocation`-variable is used for defining the exact cell coordinates where the food is located, which contributes to the moving behaviour of the ants). For further territory creation, the two-dimensional array `private Cell[][] area` is enabled with connection to the Cell class. The two values that are included in the `area` variable, are the location coordinates of each cell. At last, the `private int attraction` establishes a value for attraction, either for food or other influencing ants nearby.

The constructor `public Territory()` basically loops through the rows and columns of the territory area and checks whether or not the current cell is located at the predefined food location spot. If true, then the area variable is updated with the food coordinates and a new cell is added.
Beyond that, a `spawnAnts()` method is included in the Territory class. This function receives the ants as an arraylist (`Arraylist<Ant> Ants`), loopes through the entire list and accesses their coordinates. The coordinates are further transmitted into a new area instance with the additional property of cell occupance (which is defined in the Cell class). 
At last, a method called `printTerritory()` is implemented, in order to write the ants, the food as well as the unoccupied cells into the console. What the function basically does, is looping through the entire territory and checking each cell for occupancy (`isOccupied`) and food status (`isFood`). Therefore, ants are displayed by an "X", food by an "O" and unoccupied cells by "·".  
## [Ant class](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Ant.java)

Ants are the main protagonists in this simulation. They are hungry and moving around the territory in their search for food. They do not have names but can be identified by their position in the territory. The latter is defined as an `array` of `integers` representing X,Y coordinates of the cell the respective ant occupies. Each ant has an `age` which is measured in simulation steps as `integer`. Additionally, each ant has a life expectancy (`lifeExpectancy`), also measured in simulation steps as `integer`. ants also have a `status`, implemented as a `string`: An ant can be either `dead` (reached its life expectancy without reaching the food), `alive` (still moving through the territory), or `survived` (reached the food, and ate). 
As they determine where to walk to, the cell an ant wants to move to is defined as its next location (`nextLocation`). If that is occupied by another ant, it wants to move to an alternative next location (`alternativeLoc`). Both next and alternative location are implemented as `array` of `integers` representing X,Y coordinates.
These are the attributes of each ant: `age`, `status`, `lifeExpectancy`, `nextLocation`, `alternativeLoc`.
All attribute variables are initiated as `privat`e and accessible via getter and setter methods. 

**Ant Methods:**

An ant’s distance to the food and to other ants is calculated in the method `distCalc()` which returns the distance of the ant to the object as a `float`. The method implements Pythagoras’ theorem, as is explained under [Concepts](https://ma.reinmuth.gitlab.io/ameisen/concepts/) : “Distance and Influence”.
The angle of an ant to an object is calculated in the method `calcAngle()`, returning the angle as a `float`. This is done with a scalar product from the origin ant to another ant or the food, as described under [Concepts](https://ma.reinmuth.gitlab.io/ameisen/concepts/) : “Direction” and “Movement”.
An ant then calculates the next cell it wants to move to (`nextLocation`) based on this direction angle in the method `calcNextMove()` (returning nothing but storing the integer arrays for `nextLoc` and `alternativeLoc`). The direction calculated in `calcAngle()` has to be translated into X,Y coordinates for the ant: Each neighbor-cell of the ant’s current location is defined as a case, ranging from case 1 to case 8. As illustrated in Figure 1, each case is defined by the origin ant’s angle to the cell. The X,Y coordinates of the case corresponding to the direction angle is stored as the ant’s next location (`nextLocation`).

<img src="https://gitlab.com/ma.reinmuth/ameisen/raw/master/docu_ressource/angles1.PNG"  width="50%" height="50%">

Figure 1: Cases for next loc based on direction angle

But what if the ant cannot move to its next location? Ants have a backup plan: Their alternative next loc (`alternativeLoc`). It is determined within the method `calcNextMove()` by adding another angle-based condition to the conditions of the next location, as can be seen in Figure 2.
 
<img src="https://gitlab.com/ma.reinmuth/ameisen/raw/master/docu_ressource/angles2.PNG" width="50%" height="50%">

Figure 2: Additional anglebased conditions to determine alternative next loc

Finally, the `status` of each ant can be updated with the method `updateStatus()`: If an ant reaches its life expectancy (`lifeExpectancy`), counted in number of simulation steps, it dies. It’s `status` gets updated to `dead`. If an ant reaches the food (it’s location equals the location of the food) it eats and it’s `status` is updated to `survived`.


## [Cell class](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/Cell.java)

The territory consists of 50 * 50 indiviual cells, for which the Cell class was established. Each cell is defined by its attributes: 
	If the `boolean occupied` is `True`, an ant is occupying the cell; if it is `False` the cell is free.
	The `boolean food` is `True` only for the cell in which the food is located. It is `False` on any other cell.
	The `integer` variables `row` and `col` describe the cell’s location in the territory. They represent X- and Y-coordinates of the cell within the territory.
Those attributes have to be passed for a cell to be initialized with its constructor. As the attributes are private, `occupied` and `food` can only be accessed via methods. They can be returned with `isOccupied()` and `isFood()` and set with their corresponding setter methods `setOccupied()` and `setFood()`.

## [Main class](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/main/Main.java)

The Main class is defined as the `public static void main`, which means this is where the program starts. Main does not have any attributes, but implements all classes of our datamodel: `Ant`, `Cell`, `Territory` and `Movement`. Within Main the simulation is coordinated. Here, the ants are spawned as an arraylist of ants (`Arraylist<Ant> Ants`) and their `amount`, `attraction` and `lifeExpectancy` are set. Furthermore, the `foodLocation` and the `size` of the territory are set and the territory is created. Then the simulation is started. For each step, the movement of the ants is calculated and they move one cell – with the exceptions of those who cannot move neither to their next location (`nextLocation`) nor to their alternative next location (`alternativeLoc`) and those who have survived already by reaching the food.

## [LinePlot class](https://gitlab.com/ma.reinmuth/ameisen/blob/master/src/datamodel/LinePlot.java)

The LinePlot class is used for the instanciation of the statistical plot after expiring the ants life expectancy. A line graph is shown, where the x-axis displays the number of steps and the y-axis the number of ants which survived (got to the food location). 
