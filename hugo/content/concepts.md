---
title: Concepts
---

# Concepts

The movement of ants in our model is driven by the smell of food and other ants. Both influence types got two properties:
  1. A direction from where the influence is emitted
  2. The intensity of smell modelled as of weight of influence which declines over distance


## Influence of food

Food is the major motivation for the ants to move in our modell.
Therefore the range of influence reaches further places as well as the weight is stronger than that of ants.

If the food is located in the bottommost right corner, the topmost left corner is the only cell which is without any influence.

## Distance & Influence

Distances from an origin ant to other ants or food is calculated using the Pythagorean theorem

Distance: $$ d\_{A0 - A1} = \sqrt{(x\_{orA0} - x\_{A1})² + (y\_{A0} - y\_{A1})²} $$

where `$A0$` is the origin ant and `$A1$` is another ant or the location of food.

Therefore the maximum distance depends on the range of attraction, or for the food on the size of the territory: $$ d\_{max} = \sqrt{d_c² + d_c²} $$

where `$d_c $` is the range as amount of cells in x and y direction.

Weight: $$ a_F = 1- d_F / \sqrt{d_c² + d_c²} $$

<img src="https://gitlab.com/ma.reinmuth/ameisen/raw/master/docu_ressource/smell_food.PNG" width="50%" height="50%">

Figure 1: Influence of Food

Influence on Ants

$$
a_{Aj}=\cases{
  [1-(d\_Aj/d)^{2}]^{2}, & \text{$d_Aj$} < d,  \cr
  0,& \text{sonst.}
  }
$$

<img src="https://gitlab.com/ma.reinmuth/ameisen/raw/master/docu_ressource/ants_attraction_legend.PNG" width="50%" height="50%">

Figure 2: Ant attraction/influence


## Movement

Movement is modelled as the mean weighted direction of the influences affecting an ant. The direction can be implemented as angle or as vector

$$
\bar{a} = \frac{a_F \cdot \alpha_F + \sum\_{j=1}^n a\_{Aj} \cdot \alpha\_{Aj} }{ a_F + \sum\_{j=1}^n a\_{Aj}}
$$

### Movement exceptions

For most of the use cases, the given movement expression works fine, whereas errors occur in some special cases. These exceptional constilations have to be catched by modifications of the expression.

Exceptions:

1) Influencing ants are exactly opposite or at same direction as food & no ant with other angles is influencing (e.g. foodangle = 100° & antangle = 280°)
  $$ 
  \frac{0,3 \cdot 100 + 0,5 \cdot 280 }{ 0,3 + 0,5} = 212,5 
  $$
  The expression calculates an angle between these two input-angles, which is wrong in this case.
  When this exception is occurring, a weighted decision is implemented. The angle with the larger weight is selected as new location

2) Expression calculates outer angle in special cases (e.g. foodangle= 90° & antangle = 330°) 
  $$ 
  \frac{0,5 \cdot 90 + 0,5 \cdot 330 }{ 0,5 + 0,5} = 210
  $$
  In case of this exception, the antagle is reestablished as: 360 - antagle. For this example 360 - 330° = -30°.
  With this modification, the inner angle between the two vectorangles is derived.
  $$ 
  \frac{0,5 \cdot 100 + 0,5 \cdot -30 }{ 0,5 + 0,5} = 30
  $$
