# Ameisen Docu

Please find the documentation [__**here**__](https://ma.reinmuth.gitlab.io/ameisen/)

Run the programm by downlading the .jar file from [__**here**__](https://gitlab.com/ma.reinmuth/ameisen/raw/master/export/ABM_ants.jar)
Open cmd or terminal and execute it like this:
```sh
$ java -jar ABM_ants.jar
```

