package main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import datamodel.Ant;
import datamodel.Cell;
import datamodel.Territory;
import datamodel.Movement;
import datamodel.LinePlot;

public class Main {
	
	public static ArrayList<Ant> randomspawn(int amountAnts, int lifeExpectancy, Territory territory) {

		// list for spawned ants
		ArrayList<Ant> antList = new ArrayList<Ant>();
		// list for random locations
		ArrayList<Integer> spawnlist = new ArrayList<Integer>();
	    for ( int i = 1; i <= amountAnts; i++ ) {

			// only write unique x-values to list
			while(spawnlist.size() < amountAnts) {
				int randomY = ThreadLocalRandom.current().nextInt(1,50);
				if (!spawnlist.contains(randomY))
					spawnlist.add(randomY);
			}

			// random spawn location in row 1 with random x-value (1 to 50) from spawnlist
			int [] randomLocation = new int[]{spawnlist.get(i-1),1};
			Ant a = new Ant(randomLocation, lifeExpectancy, territory.getSize());
			antList.add(a);
			}
	    return antList;
	}
		

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ");
		System.out.println("---------- Welcome to the Agent-Based Modelling Project ----------------");
		System.out.println("---------- Simulation of ants searching for food -----------------------");
		System.out.println();
		System.out.println(" This project was done for the Class Introduction to computer science for geographers by René Westerholt");
		System.out.println("Participants: Selina Hauser, Julian krauth, marcel Reinmuth");
		System.out.println("For documentation pelase visit: https://ma.reinmuth.gitlab.io/ameisen/");
		System.out.println("# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ");
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("Initialize parameters..");
		System.out.println();
		System.out.println("Please provide the size of the territory as integer:");
		int argSize = sc.nextInt();
		System.out.println("Thank you, the territory size is: " + String.valueOf(argSize));
		System.out.println();
		System.out.println("Please provide the range of attraction ants are affecting each other as integer:");
		int argAttraction = sc.nextInt();
		System.out.println("Thank you, the range of attraction is: " + String.valueOf(argAttraction));
		System.out.println();
		System.out.println("Please provide the amount of ants which shall be randomly spawned as integer:");
		int argAmountAnts = sc.nextInt();
		System.out.println("Thank you, the amount of ants is: " + String.valueOf(argAmountAnts));
		System.out.println();
		System.out.println("Please provide the amount of timesteps which shall be simulated as integer:");
		int argSteps = sc.nextInt();
		System.out.println("Thank you, the amount of timesteps is:" + String.valueOf(argSteps));
		System.out.println();
		System.out.println("Please provide the amount of seconds which are used as delay between the timesteps:");
		int argAmountSleep = sc.nextInt();
		System.out.println("Thank you, the amount of seconds as delay is: " + String.valueOf(argAmountSleep));
		System.out.println();
		System.out.println("Would you like the program to run in verbose mode?");
		System.out.println("This means additional information on the calculation of ant movement will be printed to the console");
		System.out.println("If yes please insert 1, if no please insert 0");
		int argVerbose = sc.nextInt();
		System.out.println();
		System.out.println("Thank you, you successfully provided all parameters");
		System.out.println("Setting up Environment..");
		
		// parameters for initialization territory
		int size = argSize;
		int [] foodLocation = new int[] {size, size};
		//Ant parameters
		int amountAnts = argAmountAnts;
		int attraction = argAttraction;
		int lifeExpectancy = argSteps;
		int steps = lifeExpectancy;
		// general
		int sleep = argAmountSleep;
		int verboseLevel = argVerbose;
		
	
	
		// debugging ants
		/*int [] y = new int[]{42,0};
		Ant b = new Ant(y, lifeExpectancy);
		antList.add(b);
		int [] z = new int[]{43,0};
		Ant c = new Ant(z, lifeExpectancy);
		antList.add(c);
		int [] u = new int[]{41,0};
		Ant e = new Ant(u, lifeExpectancy);
		antList.add(e);*/		
		
		// create territory object
		Territory homeOfAnts = new Territory( size, foodLocation, attraction);
		// create Ant in random locations
		ArrayList<Ant> antList = randomspawn(amountAnts, lifeExpectancy, homeOfAnts);
		//execute spawn method
		homeOfAnts.spawnAnts(antList);
		// list to collect survived ants
		ArrayList<Ant> survivedAntList = new ArrayList<Ant>();
		
		System.out.println("Environment set up. Simulation starts in 5 seconds.");
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		ArrayList<Integer> plotX = new ArrayList<Integer>();
		ArrayList<Integer> plotY = new ArrayList<Integer>();

		int step = 0;
		// simulation starts here
		while (step <= steps){
			System.out.println("# # # # # # # # # # # # # # # # # # # # # # Step: "+step +" # #  # # # # # # # # # # # # # # # # # # # # # # ");

			homeOfAnts.printTerritory();

			Movement.calcAntsMove(antList, homeOfAnts, verboseLevel);

			Ant returnAnt = Movement.doMove(antList, homeOfAnts, step, verboseLevel);

			if (returnAnt != null) {
				survivedAntList.add(returnAnt);
				plotX.add(step);
				plotY.add(survivedAntList.size());
			}
			
			try {
				TimeUnit.SECONDS.sleep(sleep);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			step++;
		}
		
		// final territory print
		System.out.println("# # # # # # # # # # # # # # # # # # # # # # Final State # # # # # # # # # # # # # # # # # # # # # # ");

		homeOfAnts.printTerritory();

		System.out.println("Simulation done.");
		System.out.println("Amount of survived ants: " + String.valueOf(survivedAntList.size()));
		System.out.println("Amount of dead ants: " + String.valueOf(amountAnts - survivedAntList.size()));

		LinePlot frame = new LinePlot(plotX,plotY,steps);
		frame.setVisible(true);
	}
	
}
