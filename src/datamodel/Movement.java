package datamodel;

import java.util.ArrayList;

public class Movement {
	
	/**
	 * Calculation of of weight
	 *  input is attraction(max distance or food(50)) and distance from ant to object(other ant or food)
	 *  return value is float
	 */
	public static float calcWeightAnt(float antDistance, int attraction) {
		float antWeight = (float) (1 - (antDistance / (Math.sqrt(Math.pow(attraction,2) + Math.pow(attraction,2)))));
		return antWeight;
	}

	
	public static float calcWeightFood(float foodDist, int size) {
		float foodWeight = (float) (1 - (foodDist / (Math.sqrt(Math.pow(size,2) + Math.pow(size,2)))));
		return foodWeight;
	}
	
	public static float directionAngle(float foodWeight, float foodAngle, ArrayList<Float> antsAngle, ArrayList<Float> antsWeight) {
		float sumWA = 0;
		float sumWeights = 0;
		float direction = 0;
		float oneantWA = 0;

		// if no influencing ant is nearby then return fooddirection
		if (antsAngle.size() == 0) {
			direction = foodAngle;
		}
		// if other ants are influencing
		else {
			boolean angleException = false;
			for (float e : antsAngle) {
				// exception for influencing ants that are:
				//1. exactly opposite of food
				//2. in food direction
				if (e == foodAngle - 180 || e == 180 - foodAngle || e == foodAngle) {
					angleException = true;
				} else {
					angleException = false;
					break;
				}
			}
			float sum1 = 0;
			float sum2 = foodWeight;
			float weightedDecision = 0;
			if (angleException == true) {
				for (float i : antsWeight) {
					sumWeights = sumWeights + i;
					for (float e : antsAngle) {
						// if exactly opposite of food, get their weight
						if (e == foodAngle - 180 || e == 180 - foodAngle) {
							sum1 = sum1 + i;
						// if at fooddirection, get weight
						} else if (e == foodAngle) {
							sum2 = sum2 + i;
						}
					}
				}
				// check if weight is more to opposite or fooddirection
				// if more to opposite, return opposite direction
				if (sum1 > sum2) {
					for (float i : antsAngle)
						if (i != foodAngle) {
							weightedDecision = i;
							break;
						}
					direction = weightedDecision;
				}
				// if more in fooddirection, return fooddirection
				if (sum2 > sum1) {
					direction = foodAngle;
				}
			}

			// default case
			float otherAntsAngle = 0;
			if (angleException == false) {
				for (float i : antsWeight) {
					sumWeights = sumWeights + i;
					for (float e : antsAngle) {
						if (e != 0 & e != foodAngle - 180 & e != 180 - foodAngle & e != foodAngle){
							otherAntsAngle = otherAntsAngle + e;
						}
						// when influencing ants are between angle of 180 <-> 360, the angle for the looped ant is rewritten to 360
						// otherwise formula would be incorrect
						if (otherAntsAngle/antsAngle.size() >180) {
							if (e == 0) {
								e = 360;
							}
						}
						oneantWA = i * e;
						sumWA = sumWA + oneantWA;
					}
				}
				// expression took outer angle when
				// e.g. ant 12/49 with ant 11/48
				float errorcatch = (sumWA/antsAngle.size())/sumWeights;
				if (errorcatch - foodAngle >180){
					errorcatch = errorcatch -360;
					direction = ((foodWeight * foodAngle) + (errorcatch * sumWeights) / antsAngle.size()) / (foodWeight + sumWeights);
				}
				//default
				else {
					direction = ((foodWeight * foodAngle) + sumWA / antsAngle.size()) / (foodWeight + sumWeights);
				}
			}
		}
		return direction;
	}	
	
	public static void calcAntsMove(ArrayList<Ant> ants, Territory territory, int verbose) {
		// loop through ants
		for (Ant orA : ants) {
			// determine location of origin ant
			int[] originAntLocation = orA.getLocation();
			if (verbose > 0) System.out.println("______________________________");
			if (verbose > 0) System.out.println("Origin ant at: " + String.valueOf(originAntLocation[0])+ "," + String.valueOf(originAntLocation[1]));

			// new list to store ants in attraction range or just their vectors, lets see
			ArrayList<Ant> antsInRange = new ArrayList<Ant>();

			ArrayList<Float> antsDist = new ArrayList<Float>();
			ArrayList<Float> antsWeight = new ArrayList<Float>();
			ArrayList<Float> antsAngle = new ArrayList<Float>();
			
			float sumAntsAttraction = 0;

			// loop through other ants
			for (Ant otA : ants) {
				// skip origin ant

				if (ants.indexOf(otA) == ants.indexOf(orA)) {
					if (verbose > 0) System.out.println("skipping origin ant");
					continue;
				}
				int[] otherAntLocation = otA.getLocation();
				if (verbose > 0) System.out.println("------------------------------");
				if (verbose > 0) System.out.println("Other ant at: " + String.valueOf(otherAntLocation[0])+ "," + String.valueOf(otherAntLocation[1]));
				//check if ant is in range

				float distance = orA.distCalc(otherAntLocation);

				if (verbose > 0) System.out.println("Distance: " + String.valueOf(distance));

				if (distance <= territory.getAttraction()) {
					if (verbose > 0) System.out.println("Other ant is in range of attraction of origin ant");

					antsInRange.add(otA);
					antsDist.add(distance);

					float antWeight = calcWeightAnt(distance, territory.getAttraction());

					if (verbose > 0) System.out.println("Other ant attraction weight on origin ant is: " + String.valueOf(antWeight));

					antsWeight.add(antWeight);
					
					sumAntsAttraction = sumAntsAttraction + antWeight;

					float antAngle = orA.calcAngle(otA.getLocation());

					if (verbose > 0) System.out.println(antAngle);

					antsAngle.add(orA.calcAngle(otA.getLocation()));

				} else {
					if (verbose > 0) System.out.println("Other ant is out of range");
				}
			}
			if (verbose > 0) System.out.println("++++++++++++++++++++++++++++++");
			float foodDist = orA.distCalc(territory.getFoodLocation());

			if (verbose > 0) System.out.println("Food Distance: " + String.valueOf(foodDist));

			float foodWeight = calcWeightFood(foodDist, territory.getSize());

			float foodAngle = orA.calcAngle(territory.getFoodLocation());

			if (verbose > 0) System.out.println("Food Weight: " + String.valueOf(foodWeight));

			if (verbose > 0) System.out.println("Food angle: " + String.valueOf(foodAngle));

			float direction = directionAngle(foodWeight, foodAngle, antsAngle, antsWeight);

			if (verbose > 0) System.out.println("Mean weighted direction: " + String.valueOf(direction));

			orA.calcNextMove(direction, territory.getSize());

			if (verbose > 0) System.out.println("Origin Ant will move to: " + String.valueOf(orA.getNextLocation()[0]) + "," + String.valueOf(orA.getNextLocation()[1]));
			
			float influence = 0;
			// mean ant attraction 
			
			float meanAntWeight = sumAntsAttraction / (float) ants.size(); 
			// normalized mean ant attraction + food weight
			
			if (meanAntWeight > 0) {
				influence = (meanAntWeight + foodWeight) / 2;
			} else {
				
				influence = foodWeight;

			}
			// store sum of influences on the direction of the ant
			if (verbose > 0) System.out.println("Mean Ant Weight: " + String.valueOf(meanAntWeight));
			if (verbose > 0) System.out.println("Influence: " + String.valueOf(influence));

			orA.setInfluence(influence);
		}
	}
public static Ant doMove(ArrayList<Ant> ants, Territory territory, int step, int verbose) {
		
		// dummy ant variable to store return ant
		Ant returnAnt = null;
		
		for (Ant ant : ants) {
			int[] currentLocation = ant.getLocation();
			int[] nextLocation = ant.getNextLocation();
			String status = ant.getStatus();
			boolean occupation = territory.getCell(nextLocation).isOccupied();

			if (verbose > 0) System.out.println("______________________________");
			if (verbose > 0) System.out.println("Origin ant's current location: " + String.valueOf(currentLocation[0])+ "," + String.valueOf(currentLocation[1]));
			if (verbose > 0) System.out.println("Origin ant's next location: " + String.valueOf(nextLocation[0])+ "," + String.valueOf(nextLocation[1]));
			if (verbose > 0) System.out.println("Origin ant's status: " + String.valueOf(status));
			if (verbose > 0) System.out.println("Occupation flag of next location: " + String.valueOf(occupation));

			// check if ant is dead or already survived, if so don't move 'em
			if ((ant.getStatus() == "survived") || (ant.getStatus() == "dead")) {
				if (verbose > 0) System.out.println("Ant will be skipped because of it's status.");
				continue;
				// check if next Location is occupied
			}
			if ( territory.getCell(nextLocation).isOccupied() == true ) {
				if (verbose > 0) System.out.println("Ant will be skipped because of occupation of next location.");
				// don't change location
				continue;
			}
			// finally update the ants currentLocation
			ant.setLocation(nextLocation);
			if (verbose > 0) System.out.println("Ant's current location overwritten by new location");

			ant.resetNextLocation(territory.getSize());
			if (verbose > 0) System.out.println("Ant's next location resetted");

			// set new occupations
			territory.getCell(nextLocation).setOccupied(true);
			if (verbose > 0) System.out.println("Occupation flag of next location set");

			// reset old occupations
			territory.getCell(currentLocation).setOccupied(false);
			if (verbose > 0) System.out.println("Occupation flag of current location cleared");

			// update age
			ant.aging();

			if (verbose > 0) System.out.println("Ant's age updated, now at: " + String.valueOf(ant.getAge()));
			// update status of ants
			ant.updateStatus(step, territory, verbose);
			if (verbose > 0) System.out.println("Ant's status updated to: " + String.valueOf(ant.getStatus()));

			// return survived Ant to Main
			if (ant.getStatus() == "survived") {
				System.out.println("Ant status: " + String.valueOf(ant.getStatus()));
				returnAnt = ant;
			    // remove ant reference
			    ant = null;
			    }
		}
		return returnAnt;
	}
}
