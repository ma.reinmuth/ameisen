package datamodel;

import java.util.ArrayList;

/**
 * 
 * @author marcel
 * area attribute is a 2d array of cell objects
 */
public class Territory {
	private int size;
	private int [] foodLocation;
	private Cell[][] area;
	private int attraction;
	
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int[] getFoodLocation() {
		return foodLocation;
	}

	public void setFoodLocation(int[] foodLocation) {
		this.foodLocation = foodLocation;
	}

	public int getAttraction() {
		return attraction;
	}

	public void setAttraction(int attraction) {
		this.attraction = attraction;
	}
	
	public Cell[][] getArea() {
		return this.area;
	}
	
	public Cell getCell(int[] addr) {
		return this.area[addr[0]][addr[1]];
	}
	
	/**
	 * constructor
	 * @param size determines x,y extent of the territory
	 */
	public Territory(int size, int [] foodLocation, int attraction) {
		/** set size, + 1 as arrays always start their index at 0. 
		* To get a range form 1 to 50 we create an index from 0 to 50 
		 and skip the 0 in the cell creation loop later on
		*/
		this.size = size + 1;
		// set location of food
		this.foodLocation = foodLocation;
		// set attraction 
		this.attraction = attraction;
		// set area 
		this.area = new Cell[this.size][this.size];
		// loop the x dimension of the area >> column
		for (int i = 1; i < this.size; i++) {
			// loop the y dimension of the area >> row
			for (int j = 1; j < this.size; j++) {
				// check if the location is equal the determined location for food
				if ((i == foodLocation[0]) && (j == foodLocation[1])) {
					// if it is, add a cell with the attribute food set true
					this.area[i][j] = new Cell(false, true, i, j);
				} else {
					// if not, add a cell without food attribute set
					this.area[i][j] = new Cell(false,false, i, j);
				}
			}
		}
	}
	
	public void spawnAnts(ArrayList<Ant> Ants) {
		// loop through ants
		for (Ant a : Ants) {
			// get location of ant
			int[] antLocation = a.getLocation();
			this.area[antLocation[0]][antLocation[1]].setOccupied(true);
		}
	}

	public void printTerritory() {
		// loop through territory 
		for(int i = 1; i < this.size; i++) {
			for(int j = 1; j < this.size; j++) {
				// check for ant
				if (this.area[j][i].isOccupied() == true) {
					System.out.print(" X");
				// check for food
				} else if (this.area[j][i].isFood() == true) {
					System.out.print(" O");
				// print empty field
				} else {
					System.out.print(" •");
				}				
			}
			// print new line
			System.out.println();
		}
	}
}
