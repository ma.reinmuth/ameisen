package datamodel;

/**
 * This class represents a Cell of the territory.
 * @author team Ameisen
 *
 */
public class Cell {
	private boolean occupied; // when initializing the territory this parameter must be set false, when ants are spawned on the cell or move there it gets set
	private boolean food;
	private int row;
	private int col;
	
	/**
	 * Constructor to create a cell
	 * @param occupied
	 * @param food
	 */
	public Cell(boolean occupied, boolean food, int row, int col) {
		this.occupied = occupied;
		this.food = food;
		this.row = row;
		this.col = col;
	}

	public boolean isOccupied() {
		return occupied;
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

	public boolean isFood() {
		return food;
	}

	public void setFood(boolean food) {
		this.food = food;
	}
	
	
}
