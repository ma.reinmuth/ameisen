package datamodel;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JFrame;
import de.erichseifert.gral.data.DataTable;
import de.erichseifert.gral.graphics.Insets2D;
import de.erichseifert.gral.plots.XYPlot;
import de.erichseifert.gral.plots.axes.AxisRenderer;
import de.erichseifert.gral.plots.lines.DefaultLineRenderer2D;
import de.erichseifert.gral.plots.lines.LineRenderer;
import de.erichseifert.gral.plots.points.PointRenderer;
import de.erichseifert.gral.ui.InteractivePanel;

public class LinePlot extends JFrame {
    public LinePlot(ArrayList<Integer> plotX, ArrayList<Integer> plotY, int steps) {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(800, 600);

        DataTable data = new DataTable(Integer.class, Integer.class);

        data.add(0,0);
        if (plotX.size() >= 1) {
            data.add(plotX.get(0)-1,0);
            for (int i = 0; i < plotX.size(); i++) {
                data.add(plotX.get(i),plotY.get(i));
            }
            data.add(steps, plotY.get(plotY.size()-1));

        }


        XYPlot plot = new XYPlot(data);
        getContentPane().add(new InteractivePanel(plot));
        LineRenderer lines = new DefaultLineRenderer2D();
        plot.setLineRenderers(data, lines);
        Color color = new Color(0.0f, 0.3f, 1.0f);
        plot.getPointRenderers(data).get(0).setColor(color);
        plot.getLineRenderers(data).get(0).setColor(color);
        plot.getTitle().setText("Survival Timeline");
        plot.getAxisRenderer(XYPlot.AXIS_X).getLabel().setText("Movement in steps");
        plot.getAxisRenderer(XYPlot.AXIS_Y).getLabel().setText("Survived ants");

        double insetsTop = 20.0,
                insetsLeft = 60.0,
                insetsBottom = 60.0,
                insetsRight = 40.0;
        plot.setInsets(new Insets2D.Double(
                insetsTop, insetsLeft, insetsBottom, insetsRight));

    }

}