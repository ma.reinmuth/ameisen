package datamodel;


public class Ant {
	private int age;
	private int[] location;
	private int[] nextLocation;
	private int[] alternativeLoc;
	private String status;
	private int lifeExpectancy;
	private float influence;
	
	public Ant(int[] location, int lifeexpectancy, int size) {
		this.location = location;
		this.nextLocation = new int[] {1,size-1};
		this.alternativeLoc = new int[] {1,size-1};
		this.age = 0;
		this.lifeExpectancy = lifeexpectancy;
		this.status = "alive";
	}
	
	/**
	 * Calculation of distance from Ant to other object (Ant or Food)
	 * input is location of object as integer array 
	 * return value is double / float
	 */
	public float distCalc(int[] inputLocation) {
		float a_b =(float) Math.pow((this.location[0] - inputLocation[0]), 2);
		float b_c =(float) Math.pow((this.location[1] - inputLocation[1]), 2);
		float a_c = (float) Math.sqrt(a_b + b_c);
		return a_c;
	}
	/**
	 * Calculation of angle from Ant to other object (ant or food)
	 * input is location of object as integer array (ant or food)
	 * return value is angle in degree
	 */
	public float calcAngle(int[] inputLocation) {
		// vector from ant to other object (ant or food)
		int vector_x = inputLocation[0] - this.location[0];
		int vector_y = inputLocation[1] - this.location[1];
		// angle in degrees from vector product of standardvector (0,-1) and new vector
		float angle = (float) Math.toDegrees(Math.acos((-1 * vector_y)/(Math.sqrt(Math.pow(vector_x, 2) + Math.pow(vector_y, 2)))));
		// check if x-value of inputlocation is less than x-value of ant
		if (this.location[0] <= inputLocation[0]) {
			return angle;
		}
		// if false, the outer angle is searched: to get the outer angle substract angle from 360 degrees
		else {
			return 360 - angle;
		}
	}
	
	/**
	 * direction in degree will be translated into x,y movement which is then added to the current location, and updates the attribute of next location
	 * no return, attribute this.nextLocation will be updated
	 */
	public void calcNextMove(float direction, int size) {
		int oldX = this.location[0];
		int oldY = this.location[1];
		int newX = 0;
		int newY = 0;
		int altX = 0;
		int altY = 0;
		if ((direction >= 337.5) || (direction < 22.5)) {
			newX = oldX;
			newY = oldY - 1;
			//case 1
			if (direction <= 22.5) {
			    altX = oldX + 1;
			    altY = oldY - 1;
			    //altLoc = case 2
			}
			else if (direction >= 337.5) {
			    altX = oldX - 1;
			    altY = oldY - 1;
			    //altLoc = case 8
			}
		} else if ((direction >= 22.5) && (direction < 67.5)) {
			newX = oldX + 1;
			newY = oldY - 1;
			//case 2
			if (direction >= 45) {
			    altX = oldX + 1;
			    altY = oldY;
			    //altLoc = case 3
			}
			else if (direction < 45) {
			    altX = oldX;
			    altY = oldY - 1;
			    //altLoc = case 1
			}
		} else if ((direction >= 67.5) && (direction < 112.5)) {
			newX = oldX + 1;
			newY = oldY;
			//case 3
			if (direction >= 90) {
			    altX = oldX + 1;
			    altY = oldY + 1;
			    // altLoc = case 4
			}
			else if (direction < 90) {
			    altX = oldX + 1;
			    altY = oldY - 1;
			    //altLoc = case 2
			}
		} else if ((direction >= 112.5) && (direction < 157.5)) {
			newX = oldX + 1;
			newY = oldY + 1;
			//case 4
			if (direction >= 135) {
			    altX = oldX;
			    altY = oldY + 1;
			    //altLoc = case 5
			}
			else if (direction < 135) {
			    altX = oldX + 1;
			    altY = oldY;
			    //altLoc = case 3
			}
		} else if ((direction >= 157.5) && (direction < 202.5)) {
			newX = oldX;
			newY = oldY + 1;
			//case 5
			if (direction >= 180) {
			    altX = oldX - 1;
			    altY = oldY + 1;
			    //altLoc = case 6
			}
			else if (direction < 180) {
			    altX = oldX + 1;
			    altY = oldY + 1;
			    //altLoc = case 4
			}
		} else if ((direction >= 202.5) && (direction < 247.5)) {
			newX = oldX - 1;
			newY = oldY + 1;
			//case 6
			if (direction >= 225) {
			    altX = oldX -1;
			    altY = oldY;
			    //altLoc = case 7
			}
			else if (direction < 225) {
			    altX = oldX;
			    altY = oldY + 1;
			    //altLoc = case 5
			}
		} else if ((direction >= 247.5) && (direction < 292.5)) {
			newX = oldX -1;
			newY = oldY;
			//case 7
			if (direction >= 270) {
			    altX = oldX - 1;
			    altY = oldY - 1;
			    //altLoc = case 8
			}
			else if (direction < 270) {
			    altX = oldX - 1;
			    altY = oldY + 1;			    
			    //altLoc = case 6
			}
		} else { 
		//else if ((direction >= 292.5) && (direction < 337.5)) {
			newX = oldX - 1;
			newY = oldY - 1;
			//case 8
			if (direction >= 315) {
			    altX = oldX;
			    altY = oldY - 1;
			    //altLoc = case 1
			}
			else if (direction < 315) {
			    altX = oldX -1;
			    altY = oldY;
			    //altLoc = case 7
			}
		}
		// exception for case ant wants to move below the location of food
	    if (newY > (size-1)) {
	    	newY = oldY - 1;
	    	newX = oldX + 1;
	    	altX = oldX + 1;
	    	altY = oldY;
		    } else {	    
		    	this.nextLocation[0] = newX;
		    	this.nextLocation[1] = newY;
		    	
			    this.alternativeLoc[0] = altX;
			    this.alternativeLoc[1] = altY;
		    }
		    
	}

	public void updateStatus(int step, Territory territory, int verbose) {
				
		if ((this.location[0] == territory.getFoodLocation()[0]) 
				&& (this.location[1] == territory.getFoodLocation()[1])) {
			this.status = "survived";
			if (verbose > 1)System.out.println("Ant survived made it");
			territory.getCell(this.location).setOccupied(false);
			
		} else if (this.lifeExpectancy == step) {
			this.status = "dead";
			if (verbose > 1) System.out.println("Ant died");
		}
	}
	
	public void resetNextLocation(int size) {
		this.nextLocation = new int[] {1,size-1};
	}
	
	public void aging() {
		this.age = this.age + 1;
	}
	
	public void eat() {
		//event listeners?
	}
	

	public void die() {
		
	}

	public int[] getLocation() {
		return location;
	}

	public void setLocation(int[] location) {
		this.location = location;
	}


	public void setNextLocation(int[] nextLocation) {
		this.nextLocation = nextLocation;
	}
	
	public int[] getNextLocation() {
		return nextLocation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public float getInfluence() {
		return this.influence;
	}
	
	public void setInfluence(float influence) {
		this.influence = influence;
	}

}